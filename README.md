# **P**ress **R**elease **Scrape**r

# Requirements
* Python 3.5
* wget

# Install
```
git clone https://silentcon@gitlab.com/silentcon/prscrape.git
cd prscrape
```

then

`python setup.py install` or `python3 setup.py install`