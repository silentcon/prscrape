
import logging

import multiprocessing

import time

from cust_types import Dates
from prscrape import api
from scrapers import SelectorScraper

times = [2, 2]

def f(t):
    try:
        time.sleep(t)
    except:
        print('Interrupted', t)
    else:
        print('Done', t)

if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    s: SelectorScraper = api.get_scraper('waikato')(Dates('20100101', '20100115'))
    s.scrape()

