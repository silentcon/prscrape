import os
import multiprocessing
from functools import partial

import sys

import timeit

from prscrape import api
from cust_types import Dates
from scrapers import AbstractScraper
from story import Story

downloads = os.path.join(os.environ['HOMEPATH'], 'Desktop')


def download_and_open(scraper, url):
    story = scraper.scrape_story(url)
    story: Story = scraper.download(story, downloads)

    if sys.platform.startswith('linux'):
        os.subprocess.call(["xdg-open", story.html_file])
    else:
        os.startfile(story.html_file)

    return story.html_file

if __name__ =='__main__':
    start = timeit.default_timer()

    # sys.setrecursionlimit(10000)

    urls = {
        'auckland': ['https://www.auckland.ac.nz/en/about/news-events-and-notices/news/news-2017/04/jenny-kruger-s-research-funding.html'],
        'aut': ['http://www.news.aut.ac.nz/news/2017/april/aut-student-volunteers-her-way-to-russia'],
        'canterbury': ['http://canterbury.ac.nz/news/news-archive/archive-2016/main-news/uc-students-make-the-world-their-campus.html'],
        'lincoln': ['http://www.lincoln.ac.nz/News-and-Events/New-Zealands-first-Yunus-Social-Business-Centre-to-be-at-Lincoln/',
                    'http://livingheritage.lincoln.ac.nz/nodes/view/7986'],
        'massey': ['http://www.massey.ac.nz/massey/about-massey/news/article.cfm?mnarticle_uuid=F6278CBE-040E-ED8D-BE2E-923241F4A230'],
        'otago': ['http://www.otago.ac.nz/news/news/releases/otago643159.html'],
        'victoria': ['http://www.victoria.ac.nz/news/2017/04/new-zealand-poetry-given-an-italian-stage'],
        'waikato': ['http://www.waikato.ac.nz/news-events/media/2017/blessing-given-at-tauranga-cbd-campus-site']
    }

    jobs = []

    p = multiprocessing.Pool(processes=8)
    for scraper_name, obj in api.get_all_scrapers():
        scraper: AbstractScraper = obj(Dates('20170101', '20170101'))
        for url in urls.get(scraper_name):
            p.apply_async(download_and_open, (scraper, url))

    p.close()
    p.join()
    end = timeit.default_timer()
    print(end - start)

