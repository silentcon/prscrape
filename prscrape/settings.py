import os
import re

# BeautifulSoup
bs4_parser = 'lxml'
bs4_parser_backup = 'html.parser'
rate_min = 1
rate_max = 3
user_agent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.98 Safari/537.36'

# requests
requests_timeout = 20


# wget
def wget_bin(os_name: str) -> str:
    if os_name in ['win32', 'windows']:
        return os.path.join(os.path.dirname(os.path.dirname(__file__)), 'lib', 'wget.exe')
    else:
        return 'wget'


def wget_config(os_name: str) -> str:
    base = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'etc')
    filename = None
    if os_name in ['win32', 'windows']:
        filename = 'wgetrc'
    else:
        filename = 'wgetrc_unix'

    config = os.path.join(base, filename)
    return config

# timeout in seconds to prevent wget from recursing to other pages
wget_timeout = 10

# doi
DOI_REGEX_LENIENT = re.compile(r'(10\.\d{4,}/[-_;()/:A-Z0-9a-z]+(\.[-_;():A-Z0-9a-z]+)*)', re.IGNORECASE)
DOI_REGEX_NORMAL = re.compile(r'10\.\d{4,9}/[-._;()/:A-Z0-9]+', re.IGNORECASE)

# csv
csv_fieldnames = ['Story URL', 'Story Title', 'Date', 'Funding', 'Research output', 'Output Type', 'Title mentioned?',
                  'Venue mentioned?', 'Link', 'Link anchor text', 'Link Type', 'DOIs', 'OA version available',
                  'OA version linked?', 'Citation included', 'Actual Venue', 'Actual URL', 'Actual Venue (E)',
                  'Actual URL (E)', 'Actual Citation', 'Actual version OA?', 'Other output type?', 'Notes']
