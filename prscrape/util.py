import os
import re
from urllib.parse import urlparse
import sys
from typing import List

from prscrape import settings


def fix_href_url(url: str, base_url: str) -> str:
    """
    Fix href links incuding:
        1. relative links starting with /
        2. missing protocol starting with // or www
    
    :param url:
    :param base_url: the url to prepend if relative
    :return: the full url
    """
    if url.startswith('http'):
        return url
    elif url.startswith('//'):
        return 'http:' + url
    elif url.startswith('/'):
        return base_url + url
    elif urlparse(url).netloc == '':
        # wget stripped absolute url
        # replaced with relative url with '/'
        return base_url + '/' + url
    else:
        # case url = www.google.com -> http://www.google.com
        #          = facebook.com   -> http://facebook.com
        return 'http://' + url


def get_wget() -> str:
    """
    Return the included wget.exe if os is windows
    else run wget assuming wget is installed and in $PATH
    
    :return: the path to wget 
    """
    if sys.platform == 'win32':
        return settings.wget_win_exe
    else:
        return 'wget'


def remove_newline_at_end(file: str) -> None:
    """
    Removes the newline at the end. Will remove \r\n for windows
    and \n for linux
    :param file:
    """
    with open(file, 'rb+') as file:
        file.seek(-2, 2)
        file.truncate()


def sanitize_path(path, replacement='_'):
    """
    Replace invalid filename characters to underscore
    in windows
    
    :param path:
    :param replacement: The char to replace the invalid characters.
                        Underscore by default
    :return: The path with invalid filename characters replaced
    """
    return re.sub(r"[\\\/:*?<>|\r\n]", replacement, path)


def strip_protocol(url: str) -> str:
    """
    Given a url, remove the protocol part.
    Example: http://www.google.com -> www.google.com
    
    :param url: 
    :return: The url without the protocol section
    """
    u = re.sub(r'.{1,}://', '', url)
    return u
