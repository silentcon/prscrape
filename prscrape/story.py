import csv
import io
from datetime import date

import requests
from tldextract import tldextract
from typing import List, Dict

from bs4 import BeautifulSoup

from prscrape import settings
from prscrape.cust_types.csv_dialect import PRDialect


class Story:
    @property
    def soup(self):
        if self.html_file:
            self._soup = BeautifulSoup(open(self.html_file, encoding='utf-8'), settings.bs4_parser)
        elif not self._soup:
            html = requests.get(self.story_url).content
            self._soup = BeautifulSoup(html, settings.bs4_parser)

        return self._soup

    @soup.setter
    def soup(self, val: BeautifulSoup):
        self._soup = val

    def __init__(self, story_url: str, story_title: str = '', date: date = None, soup: BeautifulSoup = None, html_file=None) -> None:
        self.story_url = story_url
        self.story_title = story_title
        self.date = date
        self.soup = soup
        self.html_file = html_file
        self.links = []
        self.in_text_dois = []             # doi inside story body text

    def __str__(self):
        """Return story csv line"""
        output = io.StringIO()
        writer = csv.DictWriter(output, settings.csv_fieldnames, dialect=PRDialect.name)
        writer.writerow(self.csv_story_dict())
        return output.getvalue()

    def csv_str(self) -> str:
        """Return story and links csv lines"""
        output = io.StringIO()
        writer = csv.DictWriter(output, settings.csv_fieldnames, dialect=PRDialect.name)
        writer.writerows(self.csv_dict())
        return output.getvalue()

    @classmethod
    def empty_csv_dict(cls) -> Dict[str, str]:
        return {k: '' for _, k in enumerate(settings.csv_fieldnames)}

    def csv_dict(self) -> List[Dict[str, str]]:
        """Returns a complete list of dictionary matching each csv row"""
        dicts = [self.csv_story_dict()]
        dicts.extend(self.csv_links_dict())
        return dicts

    def csv_story_dict(self) -> Dict[str, str]:
        """Returns a dictionary matching a story"""
        d = self.empty_csv_dict()
        d['Story URL'] = self.story_url
        d['Story Title'] = self.story_title
        d['Date'] = self.date.strftime('%d/%m/%y') if self.date else ''
        d['DOIs'] = str(len(self.in_text_dois))
        return d

    def csv_links_dict(self) -> List[Dict[str, str]]:
        """Returns a list of dictionary matching a story links (DOI and non-DOI)"""
        dicts = []
        for link in self.links:
            d = self.csv_story_dict()
            d['DOIs'] = ''
            d['Link'] = link.url.strip()
            d['Link anchor text'] = link.link_text.strip() if link.link_text else 'no text'

            if isinstance(link, StoryDOILink):
                d['Link Type'] = 'DOI'

            dicts.append(d)

        return dicts

    def add_link(self, link):
        """
        :param link: link of type StoryLink 
        """
        self.links.append(link)


class StoryLink:
    def __init__(self, url: str, link_text: str):
        """
        :param url: href attribuite in a <a> tag
        :param link_text: text in between <a></>
        """
        self.url = url
        self.link_text = link_text

    def __str__(self) -> str:
        return self.url


class StoryDOILink(StoryLink):
    def __init__(self, url: str, link_text: str, doi: str):
        super(StoryDOILink, self).__init__(url, link_text)
        self.doi = doi

    def __str__(self):
        return super(StoryDOILink, self).__str__() + ', ' + self.doi