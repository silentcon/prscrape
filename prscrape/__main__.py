import logging
import multiprocessing
import os
import sys
from datetime import datetime

import click
import dateutil.parser
import requests
from typing import Tuple

from prscrape import settings
from prscrape.api import get_all_scrapers, get_scraper
from prscrape.cust_types import *


def init_logger(verbose: bool):
    format = '%(asctime)s - %(name)s - %(levelname)s - %(message)s' if verbose else '%(message)s'

    logging.basicConfig(format=format,
                        level=logging.DEBUG if verbose else logging.INFO, stream=sys.stdout)

    # hide requests logging
    logging.getLogger('requests').setLevel(logging.CRITICAL)
    logging.getLogger("urllib3").setLevel(logging.WARNING)


def validate_start_date(ctx, param, value):
    try:
        if len(value) == 4:
            value += '0101'
        elif len(value) != 8:
            raise Exception

        dateutil.parser.parse(value, dateutil.parser.parserinfo(yearfirst=True))

        return value
    except:
        raise click.BadParameter('start date needs to be in format YYYYMMDD')


def validate_end_date(ctx, param, value):
    try:
        if len(value) == 4:
            value += '1231'
        elif len(value) != 8:
            raise Exception

        dateutil.parser.parse(value, dateutil.parser.parserinfo(yearfirst=True))

        return value
    except:
        raise click.BadParameter('end date needs to be in format YYYYMMDD')


def validate_uni(value):
    if len(value) == 0:
        raise click.BadArgumentUsage('Missing university')
        pass


@click.command()
@click.option('--list', is_flag=True, help='List supported universities.')
@click.option('--start', '-s', default=datetime(2000, 1, 1).strftime('%Y%m%d'),
              callback=validate_start_date,
              help='Start date to scrape in YYYYMMDD or YYYY format. Defaults to start of year.')
@click.option('--end', '-e', default=datetime.now().strftime('%Y%m%d'),
              callback=validate_end_date,
              help='End date to scrape in YYYYMMDD or YYYY format. Defaults to end of year.')
@click.option('--csv', '-c', is_flag=True, help='Output to csv.')
@click.option('--download', '-d', is_flag=True, help='Download stories.')
@click.option('--out_dir', '-o', default='', type=click.Path(), nargs=1)
@click.option('--verbose', '-v', is_flag=True, help='Log debug.')
@click.argument('uni', nargs=-1)
def cli(list, start, end, csv: bool, download: bool, out_dir, verbose: bool, uni: Tuple[str]):
    if list:
        # list all scrapers in scraper package
        for s in get_all_scrapers():
            click.echo(s[0].lower().replace('scraper', ''))
        exit(0)

    if (csv or download) and not out_dir:
        click.echo('Output directory missing.')
        exit(1)

    validate_uni(uni)

    if out_dir:
        # accept relative paths
        if not os.path.isabs(out_dir):
            out_dir = os.path.join(os.path.curdir, out_dir)

        # make dirs if needed
        if not os.path.exists(out_dir):
            os.makedirs(out_dir)

    if 'all' in uni:
        # add all uni names, not the class themselves
        uni = [name for name, obj in get_all_scrapers()]

    # get scraper from args
    scrapers = []
    for u in uni:
        try:
            date = Dates(start, end)
        except EndBeforeStartDateException as e:
            click.echo(e)
            exit(1)

        scraper = get_scraper(u)
        if not scraper:
            click.echo('{}: Unrecognised university.'.format(u))
            click.echo('See prscrape --list.')
            exit(1)

        instance = scraper(date)
        scrapers.append(instance)

    try:
        processes = []
        for s in scrapers:
            p = multiprocessing.Process(name=s.__class__.__name__, target=scrape_job, args=(s, out_dir, csv, download, verbose))
            processes.append(p)
            p.start()

        for p in processes:
            p.join()

        click.echo('Done')
    except KeyboardInterrupt:
        pass


def scrape_job(scraper, out_dir, csv, download, verbose):
    init_logger(verbose)
    try:
        scraper.scrape(out_dir, csv, download)
    except KeyboardInterrupt:
        click.echo('Stopping {}...'.format(scraper.__class__.__name__.lower().replace('scraper', '')))
    finally:
        sys.exit()


def main():
    requests.utils.default_user_agent(settings.user_agent)
    cli()

if __name__ == '__main__':
    main()
