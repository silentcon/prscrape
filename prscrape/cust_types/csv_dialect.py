import csv


class PRDialect(csv.Dialect):
    delimiter = ','
    quotechar = '"'
    lineterminator = '\r\n'
    quoting = csv.QUOTE_ALL
    doublequote = True
    name = 'prscrape'


csv.register_dialect(PRDialect.name, PRDialect)
