from typing import List

from datetime import datetime
from dateutil import parser


class Dates:
    _start = None
    _end = None

    @property
    def start(self) -> datetime:
        return self._start

    @start.setter
    def start(self, value):
        try:
            self._start = parser.parse(value)
        except:
            raise

    @property
    def end(self) -> datetime:
        return self._end

    @end.setter
    def end(self, value):
        try:
            self._end = parser.parse(value)
        except:
            raise

    @property
    def years_between(self) -> List[int]:
        """
        Returns a list of years between the start and end dates in
        ascending order
        
        :return: a list of years between the start and end dates
        """
        return list(range(self.start.year, self.end.year + 1))

    def __init__(self, start_date: str = '20000101', end_date: str = datetime.now().strftime('%Y%m%d')) -> None:
        self.start = start_date
        self.end = end_date

        if self.end < self.start:
            raise EndBeforeStartDateException(start_date, end_date)


class LincolnDates(Dates):
    @property
    def years_between(self) -> List[int]:
        # since if current_year - 1 is included
        # we have to check both the archive and the latest news
        # as Lincoln splits them
        years_between = list(super(LincolnDates, self).years_between)
        current_year = datetime.now().year
        if current_year - 1 in years_between and current_year not in years_between:
            years_between.insert(0, current_year)

        return years_between


class EndBeforeStartDateException(Exception):
    def __init__(self, start: str, end: str):
        super(EndBeforeStartDateException, self).__init__()
        self.start = start
        self.end = end

    def __str__(self):
        return '{} is before {}'.format(self.start, self.end)

