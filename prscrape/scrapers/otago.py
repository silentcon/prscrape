import logging
import re

from bs4 import BeautifulSoup
from typing import List, Union

from prscrape import api
from prscrape.cust_types import Dates
from prscrape.story import Story, StoryLink
from prscrape.scrapers import SelectorScraper, Selectors


logger = logging.getLogger(__name__)

class OtagoScraper(SelectorScraper):
    selectors = Selectors(story_item='.newsitem',
                          href='a',
                          date_story_item='.smalltag',
                          next='',
                          story_body='#content',
                          title='.titleinner h1',
                          date_main_story='p.smalltag')

    NEWS_ITEM_HREF = re.compile(r'^otago.+\.html$', re.IGNORECASE)

    def __init__(self, dates: Dates = Dates()) -> None:
        super(OtagoScraper, self).__init__(dates)

    @property
    def name(self):
        return 'University of Otago'

    @property
    def base_url(self):
        return 'http://www.otago.ac.nz'

    @property
    def base_news_url(self) -> str:
        return 'http://www.otago.ac.nz/news/news/releases/index.html'

    @property
    def base_story_url(self):
        return 'http://www.otago.ac.nz/news/news/releases/'

    def get_urls(self, year: int) -> List[str]:
        return ["{}?year={}".format(self.base_news_url, str(year))]

    def get_story_links(self, source: Union[str, BeautifulSoup, Story], selectors: Selectors=None) -> List[StoryLink]:
        for link in super(OtagoScraper, self).get_story_links(source, selectors):
            # exclude 'A list of Otago experts available for media comment is available elsewhere on this website.'
            if link.url != ('http://www.otago.ac.nz/news/expertise/'):
                yield link

    def scrape_story(self, source: Union[str, Story], selectors: Selectors=None, overwrite=True) -> Story:
        if selectors is None:
            selectors = self.selectors

        source = super(OtagoScraper, self).scrape_story(source, selectors)

        # check title does not other markup
        # http://www.otago.ac.nz/news/news/releases/otago199824.html
        if '<!--' in source.story_title:
            source.story_title = source.soup.select_one(selectors.title).next

        return source

    def post_href(self, href: str, base: str='') -> str:
        if not base:
            base = self.base_news_url

        # news item href starting with 'otago' and ends with '.html'
        if self.NEWS_ITEM_HREF.match(href):
            return self.base_story_url + href

        return super(OtagoScraper, self).post_href(href, base)

    def download(self, story: Story, out_dir: str) -> Story:
        logger.info('Downloading %s', story.story_url)
        html_file = api.download(story.story_url, out_dir, self.base_url)
        story.html_file = html_file
        return story


