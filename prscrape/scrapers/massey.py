import datetime
import logging
from typing import List, Union, Iterator

import dateutil.parser
import requests
from bs4 import BeautifulSoup

from prscrape import api, settings
from prscrape.story import Story, StoryLink
from prscrape.cust_types import Dates
from prscrape.scrapers import SelectorScraper, Selectors


logger = logging.getLogger(__name__)


class MasseyScraper(SelectorScraper):
    selectors = Selectors(story_item='.mn_article_short',
                          date_story_item='.mn_article_summary .mn_dateshort',
                          href='.mn_article_title',
                          next='.searchFooter tr td a',  # partial selector
                          story_body='.mu-fullarticle',
                          title='title',
                          date_main_story='.mn_full_article_dates')

    def __init__(self, dates: Dates = Dates()) -> None:
        super(MasseyScraper, self).__init__(dates)

    @property
    def name(self):
        return 'Massey University'

    @property
    def base_url(self):
        return 'http://www.massey.ac.nz'

    @property
    def base_news_url(self) -> str:
        return 'http://www.massey.ac.nz/massey/about-massey/news/latest-news.cfm'

    def get_urls(self, year: int) -> Union[List[str], None]:
        # Since Massey do not categorise stories by year
        # we have to find the the page for the specific year

        # the url where we can pass in the offset to move pages
        offset_url = 'http://www.massey.ac.nz/massey/about-massey/news/latest-news.cfm?type_uuid=&category_uuid=&display_list=1&offset='
        offset = 0
        delta = 100
        date_selector = '.mn_dateshort'

        if year == datetime.datetime.now().year:
            return [offset_url + str(0)]

        while True:
            soup = BeautifulSoup(requests.get(offset_url + str(offset)).content, settings.bs4_parser)
            years = [dateutil.parser.parse(date.text).year for date in soup.select(date_selector)]
            if len(years) == 0:
                return None

            # if both years are in the page, we are sure that this is the url for the given year
            if year + 1 in years and year in years:
                # get index of first matching year
                i = years.index(year)
                return [offset_url + str(offset + i)]
            else:
                # determine if we have to go prev or next
                if years[0] > year:
                    # go next
                    delta = round(delta * 2)
                    offset += delta
                else:
                    # go prev
                    delta = round(delta / 2)
                    offset -= delta

    def get_next(self, soup: BeautifulSoup) -> Union[str, None]:
        return self.post_href(soup.select('.searchFooter tr td a')[-1]['href'])

    def get_story_links(self, source: Union[str, BeautifulSoup, Story], selectors: Selectors=None) -> Iterator[StoryLink]:
        for link in super(MasseyScraper, self).get_story_links(source, selectors):
            # Exclude Related articles
            if not link.url.startswith(self.base_news_url):
                yield link

    def post_title(self, title: str) -> str:
        return title.rsplit('-', 1)[0].strip()

    def post_href(self, href: str, base: str='') -> str:
        # eg '?mnarticle_uuid=F1ED5F45-9899-CD07-51F9-DAA761FBD8AB'
        if href.startswith('?'):
            return self.base_news_url + href

        return super(MasseyScraper, self).post_href(href, base)

    def post_date(self, date: str):
        if '|' in date:
            # example: Created: 08/12/2015 | Last updated: 08/12/2015
            tokens = date.split('|')

            # we want created date
            return tokens[0].split(':')[1].strip()
        else:
            return super(MasseyScraper, self).post_date(date)

    def stop_next(self, soup: BeautifulSoup, *args: int) -> bool:
        # since Massey do not categorise by year, we have to check when to stop
        first_story_item = soup.select(self.selectors.story_item)[0]
        date = dateutil.parser.parse(first_story_item.select_one(self.selectors.date_story_item).text)
        return date.year not in self.dates.years_between

    def download(self, story: Story, out_dir: str) -> Story:
        logger.info('Downloading %s', story.story_url)
        html_file = api.download(story.story_url, out_dir, self.base_url)
        story.html_file = html_file
        return story
