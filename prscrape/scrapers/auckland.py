import logging
from datetime import datetime

from typing import List, Union, Tuple, Iterator

from bs4 import BeautifulSoup
from prscrape.scrapers import SelectorScraper, Selectors
from prscrape.cust_types import Dates


class AucklandScraper(SelectorScraper):
    selectors = Selectors(story_item='.par .badge-text',
                          href='.linkTxt',
                          date_story_item='.greyTxt',
                          next='.next a',
                          story_body='div.par',
                          title='title',
                          date_main_story='.par .txt .listBox .greyTxt')

    def __init__(self, dates: Dates = Dates()) -> None:
        super(AucklandScraper, self).__init__(dates)

    @property
    def name(self):
        return 'University of Auckland'

    @property
    def base_url(self):
        return 'http://www.auckland.ac.nz'

    @property
    def base_news_url(self) -> str:
        # uses division slash utf-8 character
        # instead of backslash ???
        division_slash = '%E2%88%95'
        items = 100

        return 'https://www.auckland.ac.nz/en/about/news-events-and-notices/news.%E2%88%95maxItems%E2%88%95{}%E2%88%95year%E2%88%95'.format(
            items)

    def get_urls(self, year: int) -> List[str]:
        return [self.base_news_url + str(year) + '%E2%88%95.html']

    def post_title(self, title: str):
        return title.rsplit('-', 1)[0].strip()

