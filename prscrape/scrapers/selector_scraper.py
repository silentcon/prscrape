import csv
import logging
import os
import re
import time
from datetime import datetime
from random import randint

from bs4 import BeautifulSoup
import dateutil.parser
import requests
import tldextract
from typing import Iterator, Tuple, Union

from prscrape import api
from prscrape import settings
from prscrape import util
from prscrape.cust_types import Dates
from prscrape.cust_types.csv_dialect import PRDialect
from prscrape.scrapers import AbstractScraper
from prscrape.story import Story, StoryLink, StoryDOILink

logger = logging.getLogger(__name__)


class Selectors:
    """
    Object to store selectors to be used by SelectorScraper
    """

    def __init__(self, story_item: str, href: str, date_story_item: str, next: str, story_body: str, title: str, date_main_story: str, **kwargs) -> None:
        """
        :param story_item: The css selector for the short summary story item (title, date, image and summary content)
        :param href: The css selector for the element with the href attribute (the link to the main story)
        :param date_story_item: The css selector for the element containing the date inside a story item
        :param next: The css selector for the next button containing the link to the next page
        :param title: The css selector for the element the contains the title of the story in the main story page
        :param date_main_story: The css selector for the element containing the date
        """
        self.story_item = story_item
        self.href = href
        self.date_story_item = date_story_item
        self.next = next
        self.story_body = story_body
        self.title = title
        self.date_main_story = date_main_story
        self.__dict__.update(kwargs)


class SelectorScraper(AbstractScraper):
    """
    A scraper that uses predefined css selectors to scrape stories
    """
    selectors = None
    EXCLUDED_LINKS_REGEX = re.compile(r'(^javascript:)|(^tel:)|(^mailto:)|(^index.html$)')

    @property
    def base_news_url(self, *args) -> str:
        raise NotImplementedError()

    def get_urls(self, *args) -> str:
        raise NotImplementedError()

    @property
    def name(self):
        return 'Selector scraper'

    @property
    def base_url(self):
        raise NotImplementedError()

    @property
    def base_url_domain(self):
        try:
            return self._base_url_domain
        except AttributeError:
            self._base_url_domain = tldextract.extract(self.base_url).domain
            return self._base_url_domain

    def __init__(self, dates: Dates = Dates()) -> None:
        self.dates = dates

    def filter_story(self, soup: BeautifulSoup, selectors: Selectors=None) -> Tuple[bool, Union[datetime, None]]:
        try:
            if not selectors:
                selectors = self.selectors

            if selectors.date_story_item == '':
                return True, None

            date_soup = soup.select_one(selectors.date_story_item)
            if date_soup is not None:
                date = self.post_date(date_soup.text)
                date = dateutil.parser.parse(date)
                # include if between start and end dates
                return self.dates.start <= date <= self.dates.end, date
        except ValueError:
            # error parsing date
            pass
        except Exception as e:
            logger.error(e)

        return False, None

    def post_href(self, href: str, base: str='') -> str:
        if not base:
            base = self.base_url

        return util.fix_href_url(href, base)

    def get_next(self, soup: BeautifulSoup) -> Union[str, None]:
        if self.selectors.next == '':
            return

        next = soup.select_one(self.selectors.next)

        if next is not None:
            return self.post_href(next['href'])
        else:
            return

    def get_stories(self, listpage_url: str, selectors: Selectors=None) -> Iterator[Story]:
        logger.debug('Retrieving stories from %s', listpage_url)

        if selectors is None:
            selectors = self.selectors

        # get url of page with the list of stories
        res = None
        try:
            res = requests.get(listpage_url)
        except requests.exceptions.RequestException as e:
            logger.error(e)
            return

        if res.status_code != 200:
            logger.error('Got HTTP status code %d', res.status_code)
            return

        soup = BeautifulSoup(res.content, settings.bs4_parser)

        # get story items in list
        story_items = soup.select(selectors.story_item)

        if not story_items:
            # lxml might not have parsed it correctly
            # use backup parser
            soup = BeautifulSoup(res.content, settings.bs4_parser_backup)
            story_items = soup.select(selectors.story_item)

        # filter story items
        for s in story_items:
            succ, date = self.filter_story(s, selectors)
            link = self.post_href(s.select_one(selectors.href)['href'])
            if succ:
                yield Story(link, date=date)
            else:
                logger.debug('%s skipped.', link)

        # check if we can continue going to the next page
        if not self.stop_next(soup):
            # go to next page if it exists and add links
            nextpage_url = self.get_next(soup)

            # check next page does not link to the current page
            # to prevent infinite loop
            if nextpage_url and nextpage_url != listpage_url:
                yield from self.get_stories(nextpage_url)

    def scrape_story(self, source: Union[str, Story], selectors: Selectors=None, overwrite: bool=True) -> Story:
        logger.debug('Scraping story %s', source)

        story = None
        if isinstance(source, str):
            story = Story(story_url=source)
        elif isinstance(source, Story):
            story = source
        else:
            raise ValueError('Source must be a url or a Story. Got {} instead.'.format(type(source)))

        if selectors is None:
            selectors = self.selectors

        story.story_title = self.post_title(story.soup.select_one(selectors.title).text).strip()

        if overwrite or not story.date:
            date = None
            try:
                if selectors.date_main_story:
                    date_str = self.post_date(story.soup.select_one(selectors.date_main_story).text)
                    date = dateutil.parser.parse(date_str, dayfirst=True)
            except AttributeError or ValueError:
                # AttributeError - beautifulsoup returned 0 elements for date
                # ValueError - error parsing date
                date = None
                pass

            story.date = date

        # Look for doi in story body text
        story_text_doi = []
        body = story.soup.select_one(selectors.story_body)

        if body:
            for match in body.find_all(text=settings.DOI_REGEX_LENIENT):
                story_text_doi.append(settings.DOI_REGEX_LENIENT.search(match).group())

            story.in_text_dois = story_text_doi

        return story

    def get_story_links(self, source: Union[str, BeautifulSoup, Story], selectors: Selectors=None) -> Iterator[StoryLink]:
        """
        Get <a> links inside the story
        
        :param source: The story to find links
        :param selectors: The set of css selectors to use
        :return: A generator returning links
        """
        if not selectors:
            selectors = self.selectors

        soup = source
        if isinstance(source, str):
            try:
                req = requests.get(source)
            except requests.RequestException as e:
                logger.error("Error with request.")
                raise e

            soup = BeautifulSoup(req.content, settings.bs4_parser)
        elif isinstance(source, Story):
            soup = source.soup

        body = soup.select_one(selectors.story_body)
        if body:
            a_tags = body.find_all('a', href=True)
            filtered = (a for a in a_tags if not self.EXCLUDED_LINKS_REGEX.match(a.get('href')))
            for a in filtered:
                url = self.post_href(a.get('href'))
                link_text = a.text
                doi = api.find_dois(url)

                if doi:
                    yield StoryDOILink(url, link_text, doi)
                else:
                    yield StoryLink(url, link_text)

    def scrape(self, out_dir: str = None, write_csv=False, download=False):
        stories_count = 0
        base_dir, file, filepath, writer = None, None, None, None

        if out_dir:
            base_dir = os.path.join(out_dir, self.name)

        if not out_dir and write_csv:
            raise ValueError('csv is {} but out_dir missing.'.format(write_csv))

        if not out_dir and download:
            raise ValueError('download is {} but out_dir missing.'.format(download))

        try:
            if write_csv:
                filepath = os.path.join(base_dir, self.name + '.csv')

                if not os.path.exists(os.path.dirname(filepath)):
                    os.makedirs(os.path.dirname(filepath))

                # set encoding to avoid UnicodeError
                file = open(filepath, 'w', encoding='utf-8', newline='')
                writer = csv.DictWriter(file, settings.csv_fieldnames, dialect=PRDialect.name)
                writer.writeheader()
                file.flush()

            for year in self.dates.years_between:
                for url in self.get_urls(year):
                    for story in self.get_stories(url):
                        if story.date is None:
                            # story date not present in the list of page
                            # so better check story page
                            # before downloading
                            story = self.scrape_story(story)
                            if story.date is None or not (self.dates.start <= story.date <= self.dates.end):
                                continue

                        if download:
                            story = self.download(story, base_dir)

                        story = self.scrape_story(story)
                        if story.date is not None and self.dates.start <= story.date <= self.dates.end:
                            story.links.extend(list(self.get_story_links(story)))

                            if write_csv:
                                writer.writerows(story.csv_dict())

                            logger.info(story.csv_str())
                        else:
                            logger.info('Skipping story from %s', story.story_url)

                        stories_count += 1
                        time.sleep(randint(settings.rate_min, settings.rate_max))

            logger.info('Scrapped %s stories for %s.', stories_count, self.name)

            if write_csv:
                logger.info("csv saved at %s", filepath)
        except FileNotFoundError:
            logger.error("%s not a valid path.", out_dir)
        finally:
            if file:
                file.flush()
                file.close()
                util.remove_newline_at_end(filepath)

    def download(self, story: Story, out_dir: str) -> Story:
        logger.info('Downloading %s', story.story_url)
        # default to wget
        html_file = api.download_wget(story.story_url, out_dir)
        story.html_file = html_file
        return story
