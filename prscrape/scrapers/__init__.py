import os
import sys

from .abstract_scraper import AbstractScraper
from .selector_scraper import SelectorScraper, Selectors
from .auckland import AucklandScraper
from .waikato import WaikatoScraper
from .otago import OtagoScraper
from .aut import AutScraper
from .lincoln import LincolnScraper
from .victoria import VictoriaScraper
from .massey import MasseyScraper
from .canterbury import CanterburyScraper

__all__ = [
    'AucklandScraper',
    'WaikatoScraper',
    'OtagoScraper',
    'AutScraper',
    'LincolnScraper',
    'VictoriaScraper',
    'MasseyScraper',
    'CanterburyScraper'
]