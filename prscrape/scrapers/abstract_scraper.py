import logging
from abc import ABCMeta, abstractmethod
from typing import Union, IO

from prscrape.cust_types import Dates
from prscrape.story import *


class AbstractScraper(metaclass=ABCMeta):
    @property
    def name(self):
        raise NotImplementedError()

    @property
    def base_url(self):
        raise NotImplementedError()

    @property
    def base_news_url(self, *args) -> str:
        """The base url for the news stories list page"""
        raise NotImplementedError()

    @property
    def base_story_url(self, *args) -> str:
        """The base url for the main news stories page"""
        return self.base_news_url

    @abstractmethod
    def get_urls(self, *args) -> List[str]:
        """
        From the base url, form the url for getting the list of stories
        :return: The list of url to the page containing the list of stories
        """
        raise NotImplementedError()

    @abstractmethod
    def get_stories(self, *args):
        """
        Given a page with the list of stories, extract the urls to the story page

        :param args:
        :return: List of urls to the main story page
        """
        raise NotImplementedError()

    @abstractmethod
    def scrape_story(self, url: str, **kwargs) -> Story:
        """
        Scrape the story for relevant information

        :param url: The url of the story
        :return: :class:`Story <Story>` object
        """
        raise NotImplementedError()

    @abstractmethod
    def scrape(self, *args, **kwargs) -> None:
        """
        Scrape the university for stories/press releases

        :return:
        """
        raise NotImplementedError()

    def get_next(self, soup: BeautifulSoup) -> Union[str, None]:
        """
        Get the url of the next page
        
        :param soup: The beautifulsoup object of the current page
        :return: the url to the next page
        """
        raise NotImplementedError()

    def stop_next(self, soup: BeautifulSoup, *args):
        """
        For news stories not categorised by year, determine when we should stop
        getting stories in the next page

        :param soup: PyQuery object for list of stories page
        :return:
        """
        return False

    def post_title(self, title: str) -> str:
        """
        Implement to do manipulation with title
        eg title scraped has unneeded text

        :param title: the title string
        :return: the transformed title string
        """
        return title

    def filter_story(self, soup: BeautifulSoup) -> bool:
        """
        Filter story links

        :param soup: the :class:`BeautifulSoup` object for the story list page
        :return: True if included, false for excluded
        """
        return True

    def post_href(self, href: str) -> str:
        """
        Implement to do manipulation with href attribute of a tag
        for a story from the story list page

        :param href: the href attribute value
        :return: the transformed url
        """
        return href

    def post_date(self, date: str) -> str:
        """
        Implement to do manipulation with date string before parsing

        :param date: the unparsed date string
        :return: the parsable date string
        """
        return date

    def download(self, source: Union[IO, str], out_dir: str) -> str:
        """
        Download a page
        
        :param source: Can be an html file, a :class:`BeautifulSoup` object or a string url of the page
        :param out_dir: The directory to save
        :return: The path to the downloaded html file
        """
        raise NotImplementedError()
