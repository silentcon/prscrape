import logging
from typing import List

from prscrape.cust_types import Dates
from prscrape.scrapers import SelectorScraper, Selectors


class VictoriaScraper(SelectorScraper):
    """
    Stories list is not split by year
    List continues until the very first story
    """

    selectors = Selectors(story_item='.news_block',
                          href='a',
                          date_story_item='h4[itemprop=datePublished]',
                          next='.next a',
                          story_body='.entry',
                          title='title',
                          date_main_story='h6[itemprop=datePublished]')

    def __init__(self, dates: Dates = Dates()) -> None:
        super(VictoriaScraper, self).__init__(dates)

    @property
    def name(self):
        return 'Victoria University'

    @property
    def base_url(self):
        return 'http://www.victoria.ac.nz'

    @property
    def base_news_url(self) -> str:
        return 'http://www.victoria.ac.nz/news'

    def get_urls(self, year: int) -> List[str]:
        # Example url for year 2015, first page
        # http://www.victoria.ac.nz/news?filter=DCTERMS%252Eissued%3A2015-01-01..2015-12-31
        # From 2015-01-01 to 2015-12-31
        return ["{0}?filter=DCTERMS%252Eissued%3A{1}..{2}" \
                    .format(self.base_news_url, self.dates.start.strftime('%Y-%m-%d'),
                            self.dates.end.strftime('%Y-%m-%d'))]

    def post_href(self, href: str, base: str=''):
        if href.startswith('?'):
            # href for next page
            return self.base_news_url + href

        return super(VictoriaScraper, self).post_href(href, base)

    def post_title(self, title: str):
        return title.rsplit('|', 1)[0].strip()
