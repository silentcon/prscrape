from typing import List

from scrapers import SelectorScraper, Selectors
from cust_types import Dates


class TemplateScraper(SelectorScraper):
    def __init__(self, dates: Dates) -> None:
        super(UOAScraper, self).__init__(dates)
        self.selectors = Selectors(story_item='',
                                   date_story_item='',
                                   href='',
                                   next='',
                                   title='',
                                   date_main_story='')

    @property
    def name(self):
        return ''

    @property
    def base_news_url(self) -> str:
        return ''

    def get_urls(self, year: int):
        return self.base_news_url + str(year)

    def post_title(self, title: str):
        return title.rsplit('-', 1)[0].strip()


