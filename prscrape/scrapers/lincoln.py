import datetime
import logging
import re

import dateutil.parser
import requests
from bs4 import BeautifulSoup
from typing import Iterator, Union, IO, List

from prscrape import api, settings
from prscrape.cust_types import Dates, LincolnDates
from prscrape.scrapers import SelectorScraper, Selectors
from prscrape.story import Story, StoryLink


logger = logging.getLogger(__name__)


class LincolnScraper(SelectorScraper):
    LATEST_NEWS_BASE = 'http://www.lincoln.ac.nz/News-and-Events/'
    LATEST_NEWS_ALL = 'http://www.lincoln.ac.nz/News-and-Events/' + '?newssp=0&newsep=1000&f=all'
    ARCHIVE_NEWS = 'http://livingheritage.lincoln.ac.nz/nodes/view/29'

    selectors = Selectors(story_item='li.news-item-tile',
                          date_story_item='',
                          href='a.title',
                          next='',
                          story_body='article',
                          title='title',
                          date_main_story='#info')

    selectors_archive = Selectors(story_item='.ichild',
                                  date_story_item='a',
                                  href='a',
                                  next='',
                                  title='title',
                                  story_body='#main_content',
                                  date_main_story='.metadata span:nth-of-type(2)')

    ARCHIVE_POST_DATE = re.compile(r'\d{1,2} \w+ \d{4}')

    def __init__(self, dates: Dates = Dates()) -> None:
        super(LincolnScraper, self).__init__(dates)
        self.dates.__class__ = LincolnDates

    @property
    def name(self):
        return 'Lincoln University'

    @property
    def base_url(self):
        return 'http://www.lincoln.ac.nz'

    @property
    def base_news_url(self, *args):
        raise NotImplementedError

    def get_urls(self, year: int) -> List[str]:
        current_year = datetime.datetime.now().year
        if year is not None and year == current_year:
            # return latest news
            # query all news from first up to 1000, if it exists
            # should not be the case that there are more than 1000 stories
            # so this should show all the stories without to worry about
            # pagination
            return [self.LATEST_NEWS_ALL]
        else:
            # return archive
            # note current-year - 1 can be in both latest and archives

            # have to crawl the archive
            soup = BeautifulSoup(requests.get(self.ARCHIVE_NEWS).content, settings.bs4_parser)

            archive_years = soup.select('.img')

            for archive in archive_years:
                # get archive year from description
                description = archive.select_one('.desc').text
                str_year = description.split(' ')[0]
                y = dateutil.parser.parse(str_year).year

                if y == year:
                    return [archive.select_one('a')['href']]

    def get_stories(self, listpage_url: str, selectors=None) -> Iterator[Story]:
        """
        Get the stories depending on the format of the listpage_url
        Since latest stories do not put in the date in the summary,
        filtering by year needs to be done in scraping the main story page

        :param listpage_url: a valid Lincoln news url
        :param selectors:    the selectors to use
        :return:             main story urls
        """
        
        if not selectors:
            if listpage_url.startswith('http://www.lincoln.ac.nz/News-and-Events/'):
                # latest
                selectors = self.selectors
        
                # should not need to go to next since get_url returns a listpage showing all
                # stories in the last 12 months
            elif listpage_url.startswith('http://livingheritage.lincoln.ac.nz/nodes/view/'):
                # archives
                selectors = self.selectors_archive
            else:
                raise ValueError('{0} is not a valid listpage url for Lincoln'.format(listpage_url))
                
        yield from super(LincolnScraper, self).get_stories(listpage_url, selectors)

    def scrape_story(self, source: Union[str, Story], selectors: Selectors=None, overwrite: bool=False) -> Story:
        if isinstance(source, str):
            url = source
        else:
            url = source.story_url

        if url.startswith(self.LATEST_NEWS_BASE):
            source = super(LincolnScraper, self).scrape_story(source, self.selectors)
        else:
            # have to use archive selectors
            source = super(LincolnScraper, self).scrape_story(source, self.selectors_archive, overwrite=overwrite)

        if source.date is None:
            # get date from title instead
            title = source.soup.select_one(self.selectors_archive.title).text
            date_str = title.split('\r')[0]
            try:
                source.date = dateutil.parser.parse(date_str)
            except:
                pass

        return source

    def get_story_links(self, source: Union[str, Story], selectors: Selectors=None) -> Iterator[StoryLink]:
        url = source
        if isinstance(source, Story):
            url = source.story_url

        if url.startswith(self.LATEST_NEWS_BASE):
            selectors = self.selectors
        else:
            selectors = self.selectors_archive
            
        for link in super(LincolnScraper, self).get_story_links(source, selectors):
            if selectors == self.selectors_archive \
                    and link.url.startswith('http://livingheritage.lincoln.ac.nz/nodes/view'):
                continue
            yield link

    def post_title(self, title: str):
        clean_title = title.rsplit('|', 1)[0].strip()
        if 'Living Heritage' in title:
            # archive
            clean_title = clean_title.split('\n')[-1]

        return clean_title.strip()

    def post_date(self, date: str):
        match = self.ARCHIVE_POST_DATE.search(date)
        return match.group() if match else super(LincolnScraper, self).post_date(date)

    def download(self, story: Story, out_dir: str) -> Story:
        logger.info('Downloading %s', story.story_url)
        html_file = ''
        if story.story_url.startswith('http://livingheritage.lincoln.ac.nz'):
            html_file = api.download(story.story_url, out_dir, 'http://livingheritage.lincoln.ac.nz')
        else:
            html_file = api.download_wget(story.story_url, out_dir)

        story.html_file = html_file
        return story
