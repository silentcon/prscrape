import logging
from datetime import datetime

import requests
from typing import Tuple, List, Union, Iterator

from bs4 import BeautifulSoup
import dateutil.parser

from prscrape import settings
from prscrape.story import Story, StoryLink
from prscrape.scrapers import SelectorScraper, Selectors
from prscrape.cust_types import Dates


class AutScraper(SelectorScraper):
    selectors = Selectors(story_item='#mainContent div li',
                          href='a',
                          date_story_item='.date',
                          next='',
                          story_body='#mainContent',
                          title='title',
                          date_main_story='#mainContent div .date')

    def __init__(self, dates: Dates = Dates()) -> None:
        super(AutScraper, self).__init__(dates)

    @property
    def name(self):
        return 'AUT'

    @property
    def base_url(self):
        return 'http://www.news.aut.ac.nz'

    @property
    def base_news_url(self) -> str:
        return 'http://www.news.aut.ac.nz/view-all-news'

    def get_urls(self, *args) -> List[str]:
        return [self.base_news_url]

    def post_title(self, title: str):
        return title.rsplit('-', 1)[0].strip()

    def stop_next(self, soup: BeautifulSoup, *args):
        return True

    def filter_story(self, soup: BeautifulSoup, selectors: Selectors = None) -> Tuple[bool, Union[datetime, None]]:
        """
        Filter stories based on year
        Filter links that do not lead to a story in aut

        :param soup: BeautifulSoup object for the list of stories page
        :param selectors: The css selectors to use
        :return: True if it contains with 'http://www.news.aut.ac.nz/news', otherwise, false
        """
        if not selectors:
            selectors = self.selectors

        date = dateutil.parser.parse(soup.select_one(selectors.date_story_item).text)
        link = soup.select_one(selectors.href)['href']
        if not link.startswith('http://www.news.aut.ac.nz/news') or date.year not in self.dates.years_between:
            return False, None
        else:
            return super(AutScraper, self).filter_story(soup)

    def get_story_links(self, source: Union[str, BeautifulSoup, Story], selectors: Selectors=None) -> Iterator[StoryLink]:
        soup = None
        if isinstance(source, str):
            res = requests.get(source)
            soup = BeautifulSoup(res.content, settings.bs4_parser)
        elif isinstance(source, BeautifulSoup):
            soup = source
        elif isinstance(source, Story):
            soup = source.soup
        else:
            raise TypeError('source must be of type str, BeautifulSoup or Story')

        # remove tweet and academic calendar links
        el = soup.select_one('.tweetshare')
        if el:
            el.decompose()

        el = soup.select_one('.publishedDate')
        if el:
            el.decompose()

        yield from super(AutScraper, self).get_story_links(soup, selectors)
