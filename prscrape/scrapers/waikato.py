import re

import logging
from datetime import datetime

import requests
from typing import Tuple, List, Iterator, Union, IO

from dateutil import parser
from bs4 import BeautifulSoup

from prscrape import settings
from prscrape.scrapers import SelectorScraper, Selectors
from prscrape.cust_types import Dates
from prscrape.story import Story, StoryLink


logger = logging.getLogger(__name__)


class WaikatoScraper(SelectorScraper):
    BASE_URL_ARCHIVE = 'http://www.waikato.ac.nz/news/archive.shtml'
    regex_title_date_news_old = re.compile(r'(?<=\()\d{1,2} \w+ \d{4}(?=\))')
    regex_title_date_news = re.compile(r'(?<=\()\d{1,2} \w+ \d{4}(?=\)$)')

    selectors = Selectors(story_item='.media',
                          date_story_item='.media-body .media-heading',  # inside title
                          href='.media-body .media-heading a',
                          next='a.btn.btn-default.active.pull-right',
                          story_body='section#content',
                          title='title',
                          date_main_story='#content h3')

    # pre 2009
    selectors_archive = Selectors(story_item='#content p',
                                  date_story_item='span.address',
                                  href='a',
                                  next='',  # all in one page
                                  story_body=selectors.story_body,
                                  title=selectors.title,
                                  date_main_story='#content p strong')

    selectors_news_old = Selectors(story_item=selectors_archive.story_item,
                                   date_story_item='p',
                                   href='a',
                                   next='',
                                   story_body='#content',
                                   title=selectors.title,
                                   date_main_story=selectors.date_main_story)

    def __init__(self, dates: Dates = Dates()) -> None:
        super(WaikatoScraper, self).__init__(dates)

    @property
    def name(self):
        return 'University of Waikato'

    @property
    def base_url(self):
        return 'http://www.waikato.ac.nz'

    @property
    def base_news_url(self):
        return 'http://www.waikato.ac.nz/news-events/media/'

    def get_urls(self, year: int) -> List[str]:
        """
        Get the url of the page to scrape.

        The University of Waikato stories are grouped by year
        baseUrl/year

        :param year: The publish year of the stories
        :return: The resulting url
        """
        # 2009 is in both archive and non-archive
        urls = []
        if year >= 2011:
            urls = [self.base_news_url + str(year)]
        elif year == 2010:
            # http://www.waikato.ac.nz/news-events/media/2010.shtml
            urls = ['{}/news-events/media/{}.shtml'.format(self.base_url, str(year))]
        else:
            urls = ['{}.shtml?year={}'.format(self.BASE_URL_ARCHIVE, str(year))]

        if year == 2009:
            # two 2009 pages
            urls.insert(0, '{}.shtml?year={}'.format(self.BASE_URL_ARCHIVE, str(year)))

        return urls
        
    def get_stories(self, listpage_url: str, selectors=None) -> Iterator[str]:
        if not selectors:
            if listpage_url.startswith(self.BASE_URL_ARCHIVE):
                selectors = self.selectors_archive
            elif listpage_url.startswith(self.base_news_url):
                selectors = self.selectors

                if '2009' in listpage_url or '2010' in listpage_url:
                    selectors = self.selectors_news_old

        yield from super(WaikatoScraper, self).get_stories(listpage_url, selectors)

    def get_story_links(self, source: Union[str, BeautifulSoup, Story], selectors: Selectors=None) -> Iterator[StoryLink]:
        soup = None
        if isinstance(source, str):
            res = requests.get(source)
            soup = BeautifulSoup(res.content, settings.bs4_parser)
        elif isinstance(source, BeautifulSoup):
            soup = source
        elif isinstance(source, Story):
            soup = source.soup
        else:
            raise TypeError('source must be of type str, BeautifulSoup or Story')

        # remove share links
        for s in soup.select('.share-buttons'):
            s.decompose()

        yield from super(WaikatoScraper, self).get_story_links(soup)

    def scrape_story(self, source: Union[str, Story], selectors=None, overwrite=True) -> Story:
        if not selectors:
            url = source
            if isinstance(source, Story):
                url = source.story_url
            
            if url.startswith(self.BASE_URL_ARCHIVE):
                selectors = self.selectors_archive
            elif url.startswith(self.base_news_url):
                selectors = self.selectors

                if '2009' in url or '2010' in url:
                    selectors = self.selectors_news_old

        return super(WaikatoScraper, self).scrape_story(source, selectors)

    def filter_story(self, soup: BeautifulSoup, selectors: Selectors=None) -> Tuple[bool, Union[datetime, None]]:
        if not selectors:
            selectors = self.selectors

        if selectors == self.selectors or selectors == self.selectors_news_old:
            # get date from story item title
            # title has a tag with href
            match = None
            if selectors == self.selectors_news_old:
                match = self.regex_title_date_news_old.search(soup.text)
            else:
                story_title_el = soup.select_one(self.selectors.date_story_item)

                if story_title_el:
                    # date at the end of title
                    # example:
                    #     Celebrating our distinguished alumni (30 March 2017)
                    #     NZ retailers urged to follow Japan’s online success story (9 March 2017)
                    match = self.regex_title_date_news.search(story_title_el.text)

            if match:
                # there is a match
                date_str = match.group()
                date = parser.parse(date_str)
                try:
                    return self.dates.start <= date <= self.dates.end, date
                except:
                    return False, None

            return False, None
        else:
            # archive
            return super(WaikatoScraper, self).filter_story(soup, selectors)

    def post_title(self, title: str):
        """
        Title in <title> is appended with ' - News Stories: University of Waikato'.
        This method removes it.

        :param title: the text inside <title>
        :return: the title with ' - News Stories: University of Waikato' removed.
        """
        if '- News Stories : University of Waikato' in title:
            return title.rsplit('-', 1)[0].strip()
        elif ': University of Waikato' in title:
            return title.replace(': University of Waikato', '')
        else:
            return title;
