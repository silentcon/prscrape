import logging

import requests
from typing import List, Union, Iterator

from bs4 import BeautifulSoup

from prscrape import settings
from prscrape.story import Story, StoryLink
from prscrape.scrapers import SelectorScraper, Selectors
from prscrape.cust_types import Dates


class CanterburyScraper(SelectorScraper):
    selectors = Selectors(story_item='.col-xs-8',
                          date_story_item='.datetime',
                          href='.feature-title a',
                          next='.news-pagination a',  # partial
                          story_body='#main',
                          title='#main h1',
                          date_main_story='#main p')

    def __init__(self, dates: Dates = Dates()) -> None:
        super(CanterburyScraper, self).__init__(dates)

    @property
    def name(self):
        return 'University of Canterbury'

    @property
    def base_url(self):
        return 'http://www.canterbury.ac.nz'

    @property
    def base_news_url(self) -> str:
        return 'http://www.canterbury.ac.nz/news/news-archive/archive'

    def get_urls(self, year: int) -> List[str]:
        return ["{}-{}".format(self.base_news_url, str(year))]

    def get_next(self, soup: BeautifulSoup) -> Union[str, None]:
        pages = soup.select(self.selectors.next)

        if pages:
            next = [x for x in pages if x.text == '>']

            if next:
                return self.post_href(next[0]['href'])

        return None

    def stop_next(self, soup: BeautifulSoup, *args) -> bool:
        # with last button
        # ['<<', '<', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '20', '>', '>>']
        # when on the last page
        # ['<<', '<', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19']
        pagination = soup.select(self.selectors.next)
        next = [x for x in pagination if x.text == '>>']

        # stop if the last button is not found (next is empty)
        return len(next) == 0

    def get_story_links(self, source: Union[str, BeautifulSoup, Story], selectors: Selectors=None) -> Iterator[StoryLink]:
        # remove 'What to read next' section to exclude links from it
        soup = None
        if isinstance(source, str):
            html = requests.get(source).content
            soup = BeautifulSoup(html, settings.bs4_parser)
        elif isinstance(source, Story):
            soup = source.soup

        read_next = soup.select_one('.more-news-wrapper')
        if read_next:
            read_next.decompose()

        yield from super(CanterburyScraper, self).get_story_links(soup, selectors)
