import unittest
from typing import List

import requests
from bs4 import BeautifulSoup
from datetime import date
from parameterized import parameterized

from prscrape import settings
from prscrape.cust_types import Dates
from prscrape.api import get_scraper
from prscrape.scrapers import WaikatoScraper, SelectorScraper
from prscrape.story import StoryLink, StoryDOILink


class TestScraper(object):
    scraper_name: str = 'selector'

    # test_get_stories
    stories_count_2015 = 0

    # test_filter_story
    stories_count_2015_filtered = 0

    # test_get_next
    first_next_url = ''

    # test_scrape_story
    scrape_story_url = ''
    scrape_story_title = ''
    scrape_story_year: int = None
    scrape_story_month: int = None
    scrape_story_day: int = None

    # test_get_story_links_none
    story_no_links = ''

    # test_get_story_links_doi
    story_doi_link = ''
    story_doi_actual_link = ''
    story_num_links: int = None
    story_doi = ''

    def setUp(self):
        self.scraper: SelectorScraper = get_scraper(self.scraper_name)()

    def test_get_url(self):
        years = list(reversed(range(2015, 2017)))
        for y in years:
            url = self.scraper.get_urls(y)
            self.assertIsNotNone(url, 'should get at least one url')
            self.assertGreater(len(url), 0, 'should get at least one url')
            self.assertIn(str(y), url[0], 'year should be in url')
            response = requests.get(url[0])
            self.assertEqual(response.status_code, 200)

    def test_get_stories(self):
        stories = list(self.scraper.get_stories(self.scraper.get_urls(2015)[0]))
        self.assertIsNotNone(stories)
        self.assertEqual(len(stories), self.stories_count_2015, 'should get correct number of stories.')

    def test_filter_story(self):
        self.scraper = get_scraper(self.scraper_name)(Dates('20150601', '20150630'))
        stories = list(self.scraper.get_stories(self.scraper.get_urls(2015)[0]))
        self.assertIsNotNone(stories)
        self.assertEqual(len(stories), self.stories_count_2015_filtered, 'should get correct number of stories.')

    def test_get_next(self):
        response = requests.get(self.scraper.get_urls(2015)[0])
        soup = BeautifulSoup(response.content, settings.bs4_parser)
        next_url = self.scraper.get_next(soup)
        self.assertEqual(self.first_next_url,
                         next_url)
        pass

    def test_scrape_story(self):
        story = self.scraper.scrape_story(self.scrape_story_url)
        self.assertEqual(self.scrape_story_url, story.story_url)
        self.assertEqual(self.scrape_story_title, story.story_title, 'story title should match')
        self.assertEqual(self.scrape_story_year, story.date.year, 'story year should match')
        self.assertEqual(self.scrape_story_month, story.date.month, 'story month should match')
        self.assertEqual(self.scrape_story_day, story.date.day, 'story day should match')
        pass

    def test_get_story_links_none(self):
        links: List[StoryDOILink] = list(self.scraper.get_story_links(self.story_no_links))
        self.assertEqual([], links)

    def test_get_story_links_doi(self):
        links: List[StoryDOILink] = list(self.scraper.get_story_links(self.story_doi_link))
        doi_link: StoryDOILink = None
        for l in links:
            if isinstance(l, StoryDOILink):
                doi_link = l
                break

        self.assertIsNotNone(doi_link)
        self.assertEqual(len(links), self.story_num_links)
        self.assertIsInstance(doi_link, StoryDOILink)
        self.assertEqual(doi_link.url, self.story_doi_actual_link)
        self.assertEqual(doi_link.doi, self.story_doi)


class TestAucklandScraper(TestScraper, unittest.TestCase):
    scraper_name = 'auckland'
    stories_count_2015 = 367
    stories_count_2015_filtered = 26
    first_next_url = 'https://www.auckland.ac.nz/en/about/news-events-and-notices/news.%E2%88%95maxItems%E2%88%95100%E2%88%95par_newssearchlist_start%E2%88%95100%E2%88%95year%E2%88%952015%E2%88%95.html'
    scrape_story_url = 'https://www.auckland.ac.nz/en/about/news-events-and-notices/news/news-2015/11/auckland-celebrates-17-million-success-in-marsden-fund.html'
    scrape_story_title = 'Auckland celebrates $17 million success in Marsden Fund'
    scrape_story_year = 2015
    scrape_story_month = 11
    scrape_story_day = 5
    story_no_links = 'https://www.auckland.ac.nz/en/about/news-events-and-notices/news/news-2015/09/public-lecture-on-the-u-s--presidential-elections.html'
    story_doi_link = 'https://www.auckland.ac.nz/en/about/news-events-and-notices/news/news-2015/12/students_-self-belief-and-goal-setting-study-has-surprising-resu.html'
    story_doi_actual_link = 'http://onlinelibrary.wiley.com/doi/10.1111/bjep.12103/abstract'
    story_num_links = 1
    story_doi = '10.1111/bjep.12103'


class TestAut(TestScraper, unittest.TestCase):
    scraper_name = 'aut'
    stories_count_2015 = 117    # all stories in one page so can't test
    stories_count_2015_filtered = 9  # first story in june 2015 is dated july
    first_next_url = ''
    scrape_story_url = 'http://www.news.aut.ac.nz/news/2015/june/north-shore-scientists-launch-major-global-health-study'
    scrape_story_title = 'AUT scientists launch major global health study'
    scrape_story_year = 2015
    scrape_story_month = 6
    scrape_story_day = 22
    story_no_links = 'http://www.news.aut.ac.nz/news/2015/may/aut-business-school-postgraduate-students-share-insight-on-path-less-travelled'
    story_doi_link = 'http://www.news.aut.ac.nz/news/2015/october/government-plan-to-tackle-obesity-addresses-only-half-the-problem-aut-nutrition-expert'
    story_doi_actual_link = 'http://www.aut.ac.nz/profiles/sport-recreation/professors/elaine-rush2' # not a journal doi
    story_num_links = 7
    story_doi = '10.1017/S0007114516004372'

    def test_get_stories(self):
        pass

    def test_get_url(self):
        pass

    def test_get_next(self):
        pass


class TestCanterbury(TestScraper, unittest.TestCase):
    scraper_name = 'canterbury'
    stories_count_2015 = 254
    stories_count_2015_filtered = 20
    first_next_url = 'http://www.canterbury.ac.nz/news/news-archive/archive-2015/2/'
    scrape_story_url = 'http://canterbury.ac.nz/news/news-archive/archive-2015/main-news/former-mp-examines-the-politics-of-education-.html'
    scrape_story_title = 'Former MP examines \'The Politics of Education\''
    scrape_story_year = 2015
    scrape_story_month = 11
    scrape_story_day = 17
    story_no_links = 'http://canterbury.ac.nz/news/news-archive/archive-2015/main-news/two-pasifika-uc-student-teachers-win-scholarships.html'
    story_doi_link = 'http://www.canterbury.ac.nz/news/news-archive/archive-2015/2015-engineering-news/uc-student-working-with-top-android-designer.html'
    story_doi_actual_link = 'http://journal.frontiersin.org/article/10.3389/fpsyg.2015.00883/abstract'
    story_num_links = 2
    story_doi = '10.3389/fpsyg.2015.00883'


class TestLincolnRecent(TestScraper, unittest.TestCase):
    # ignore 2015, this is for the most recent year
    scraper_name = 'lincoln'
    stories_count_2015 = 22
    # stories_count_2015_filtered = 3
    first_next_url = ''
    scrape_story_url = 'http://www.lincoln.ac.nz/News-and-Events/New-Zealands-first-Yunus-Social-Business-Centre-to-be-at-Lincoln/'
    scrape_story_title = 'New Zealand’s first Yunus Social Business Centre to be at Lincoln'
    scrape_story_year = 2017
    scrape_story_month = 4
    scrape_story_day = 4
    story_no_links = 'http://www.lincoln.ac.nz/News-and-Events/New-Zealands-first-Yunus-Social-Business-Centre-to-be-at-Lincoln/'
    story_doi_link = 'http://www.lincoln.ac.nz/News-and-Events/Research-pops-cork-on-lunareffect-wine-theory/'
    story_doi_actual_link = 'http://journals.plos.org/plosone/article?id=10.1371/journal.pone.0169257'
    story_num_links = 2
    story_doi = '10.1371/journal.pone.0169257'  # journal collection of dois

    def test_get_url(self):
        pass

    def test_get_next(self):
        pass

    def test_get_stories(self):
        stories = list(self.scraper.get_stories(self.scraper.get_urls(2017)[0]))
        self.assertIsNotNone(stories)
        self.assertGreater(len(stories), self.stories_count_2015, 'should get correct number of stories.')

    def test_filter_story(self):
        # have to go into story to get date...
        pass


class TestLincolnArchive(TestScraper, unittest.TestCase):
    scraper_name = 'lincoln'
    stories_count_2015 = 140
    stories_count_2015_filtered = 14
    first_next_url = ''
    scrape_story_url = 'http://livingheritage.lincoln.ac.nz/nodes/view/7743'
    scrape_story_title = 'Chance for Chinese students to Study Abroad'
    scrape_story_year = 2015
    scrape_story_month = 9
    scrape_story_day = 4
    story_no_links = 'http://livingheritage.lincoln.ac.nz/nodes/view/7816'
    story_doi_link = 'http://livingheritage.lincoln.ac.nz/nodes/view/7681'
    story_doi_actual_link = 'http://onlinelibrary.wiley.com/journal/10.1111/%28ISSN%291745-4549'
    story_num_links = 4
    story_doi = '10.1111/(ISSN)1745-4549'   # journal collection of dois

    def test_get_url(self):
        pass

    def test_get_next(self):
        pass


class TestMassey(TestScraper, unittest.TestCase):
    scraper_name = 'massey'
    stories_count_2015 = 616
    stories_count_2015_filtered = 62
    first_next_url = 'http://www.massey.ac.nz/massey/about-massey/news/latest-news.cfm?type_uuid=&category_uuid=&display_list=1&offset=891'
    scrape_story_url = 'http://www.massey.ac.nz/massey/about-massey/news/article.cfm?mnarticle_uuid=212E1B1E-ADCF-1A46-2AAD-62895F1FCBA7'
    scrape_story_title = 'Classics hit Auckland campus in 2016'
    scrape_story_year = 2015
    scrape_story_month = 12
    scrape_story_day = 14
    story_no_links = 'http://www.massey.ac.nz/massey/about-massey/news/article.cfm?mnarticle_uuid=212E1B1E-ADCF-1A46-2AAD-62895F1FCBA7'
    story_doi_link = 'http://www.massey.ac.nz/massey/about-massey/news/article.cfm?mnarticle_uuid=BB6B8162-EEAE-188C-8699-8D0FFD5ED5ED'
    story_doi_actual_link = 'http://rspa.royalsocietypublishing.org/lookup/doi/10.1098/rspa.2015.0390'
    story_num_links = 1
    story_doi = '10.1098/rspa.2015.0390'

    def test_get_url(self):
        url = self.scraper.get_urls(2015)[0]
        self.assertEqual('http://www.massey.ac.nz/massey/about-massey/news/latest-news.cfm?type_uuid=&category_uuid=&display_list=1&offset=881',
                         url, '2015 starts on offset 881')

    def test_get_stories(self):
        self.scraper = get_scraper(self.scraper_name)(Dates('20150101', '20151231'))
        stories = []
        for s in self.scraper.get_stories(self.scraper.get_urls(2015)[0]):
            stories.append(s)


class TestOtago(TestScraper, unittest.TestCase):
    scraper_name = 'otago'
    stories_count_2015 = 205
    stories_count_2015_filtered = 15
    first_next_url = ''
    scrape_story_url = 'http://www.otago.ac.nz/news/news/releases/otago422604.html'
    scrape_story_title = 'Wellington start-up in top three at Paris Climate Conference'
    scrape_story_year = 2015
    scrape_story_month = 12
    scrape_story_day = 21
    story_no_links = 'http://www.otago.ac.nz/news/news/releases/otago422604.html'
    story_doi_link = 'http://www.otago.ac.nz/news/news/releases/otago385002.html'
    story_doi_actual_link = 'http://bmjopen.bmj.com/content/5/12/e008409.full'
    story_num_links = 1
    story_doi = '10.1136/bmjopen-2015-008409'

    def test_get_next(self):
        pass


class TestVictoria(TestScraper, unittest.TestCase):
    scraper_name = 'victoria'
    stories_count_2015 = 424
    stories_count_2015_filtered = 45
    first_next_url = 'http://www.victoria.ac.nz/news?filter=DCTERMS%252Eissued%3A2015-01-01..2015-12-31&start=10'
    scrape_story_url = 'http://www.victoria.ac.nz/news/2015/12/singing-and-student-wellbeing-researched'
    scrape_story_title = 'Singing and student wellbeing researched'
    scrape_story_year = 2015
    scrape_story_month = 12
    scrape_story_day = 16
    story_no_links = 'http://www.victoria.ac.nz/news/2015/12/singing-and-student-wellbeing-researched'
    story_doi_link = 'http://www.victoria.ac.nz/news/2015/11/research-shows-reality-of-runaway-ice-loss-in-antarctica'
    story_doi_actual_link = 'http://www.nature.com/ncomms/2015/151126/ncomms9910/full/ncomms9910.html'
    story_num_links = 4
    story_doi = '10.1038/ncomms9910'

    def test_get_url(self):
        pass

    def test_get_stories(self):
        self.scraper = get_scraper('victoria')(Dates('20150101', '20151231'))
        super(TestVictoria, self).test_get_stories()

    def test_get_next(self):
        self.scraper = get_scraper('victoria')(Dates('20150101', '20151231'))
        super(TestVictoria, self).test_get_next()


class TestWaikatoNew(TestScraper, unittest.TestCase):
    scraper_name = 'waikato'
    stories_count_2015 = 330
    stories_count_2015_filtered = 40
    first_next_url = 'http://www.waikato.ac.nz/news-events/media/2015?result_256243_result_page=2'
    scrape_story_url = 'http://www.waikato.ac.nz/news-events/media/2015/academic-audit-for-waikato-university-released'
    scrape_story_title = 'Academic audit for Waikato University released'
    scrape_story_year = 2015
    scrape_story_month = 12
    scrape_story_day = 4
    story_no_links = 'http://www.waikato.ac.nz/news-events/media/2015/academic-audit-for-waikato-university-released'
    story_doi_link = 'http://www.waikato.ac.nz/news-events/media/2015/climate-change-is-warming-our-lakes-and-fast'
    story_doi_actual_link = 'http://www.nature.com/articles/sdata20158'
    story_num_links = 3
    story_doi = '10.1038/sdata.2015.8'


class TestWaikato2010(TestScraper, unittest.TestCase):
    scraper_name = 'waikato'
    stories_count_2015 = 364         # 2010 contains two 2009 stories...
    stories_count_2015_filtered = 33
    first_next_url = ''
    scrape_story_url = 'http://www.waikato.ac.nz/news-events/media/2010/12application-numbers-up-at-waikato-universitys-tauranga-campus.shtml'
    scrape_story_title = 'Application numbers up at Waikato University’s Tauranga campus'
    scrape_story_year = 2010
    scrape_story_month = 12
    scrape_story_day = 13
    # story_no_links = 'http://www.waikato.ac.nz/news-events/media/2010/12application-numbers-up-at-waikato-universitys-tauranga-campus.shtml'
    # story_doi_link = 'http://www.waikato.ac.nz/news-events/media/2015/climate-change-is-warming-our-lakes-and-fast'
    # story_doi_actual_link = 'http://www.nature.com/articles/sdata20158'
    # story_num_links = 3
    # story_doi = '10.1038/sdata.2015.8'

    def test_get_stories(self):
        stories = list(self.scraper.get_stories(self.scraper.get_urls(2010)[0]))
        self.assertIsNotNone(stories)
        self.assertEqual(len(stories), self.stories_count_2015, 'should get correct number of stories.')

    def test_filter_story(self):
        self.scraper = get_scraper(self.scraper_name)(Dates('20100601', '20100630'))
        stories = list(self.scraper.get_stories(self.scraper.get_urls(2010)[0]))
        self.assertIsNotNone(stories)
        self.assertEqual(len(stories), self.stories_count_2015_filtered, 'should get correct number of stories.')

    def test_get_next(self):
        pass

    def test_get_story_links_none(self):
        pass

    def test_get_story_links_doi(self):
        pass

