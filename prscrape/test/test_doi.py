import os
import unittest

from parameterized import parameterized

from prscrape import settings
from prscrape.api import find_dois


class TestDOI(unittest.TestCase):
    regex = settings.DOI_REGEX_LENIENT

    should_match = [(l, True) for l in
                    open(os.path.abspath(os.path.join(os.path.dirname(__file__), 'files', 'doi_dump.txt')), 'r').readlines()]

    @parameterized.expand(
        should_match +
        # should fail
        [(l, False) for l in [
            '',
            'doi:',
            '10/jquery',
            '10/23423/jquery',
            'doi:10.1093'
            'doi:10.1093/',
            'doi:10.1093'
        ]]
    )
    def test_regex(self, line, should_match):
        match = self.regex.search(line)
        self.assertEqual(match is not None, should_match, '{} should {} match'.format(line, '' if should_match else 'not'))

    @parameterized.expand([
        ('http://google.com', None),                                                                      # no doi
        ('http://rspb.royalsocietypublishing.org/content/277/1688/1643', '10.1098/rspb.2009.2202'),       # one doi match
        ('https://link.springer.com/article/10.1007%2Fs00114-008-0425-5', '10.1007/s00114-008-0425-5')    # multiple doi matches
    ])
    def test_webpage(self, url: str, doi_to_find: str or None):
        dois = find_dois(url)

        # capture match only
        if doi_to_find:
            self.assertIsNotNone(dois, 'Returned no dois when expecting {}'.format(doi_to_find))
            self.assertTrue(doi_to_find in dois, 'Supposed to find {} in {}'.format(doi_to_find, url))
        else:
            self.assertEqual(dois, [])

if __name__ == '__main__':
    unittest.main()
