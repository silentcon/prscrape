import glob
import inspect
import logging
import os
import re
import shutil
import sys
from collections import OrderedDict
from subprocess import call, DEVNULL
from urllib.parse import urlparse
from urllib.request import urlretrieve

import bs4
import requests
from bs4 import BeautifulSoup
from tldextract import tldextract
from typing import Union, Callable, List, Tuple

from prscrape import scrapers, settings
from prscrape.util import sanitize_path, strip_protocol

logger = logging.getLogger(__name__)
INVALID_DOI_URLS = re.compile(r'(^(mailto|tel):)|(\.(pdf)$)')


def download(source: Union[str, BeautifulSoup], out_dir: str, domain='') -> str:
    """
    Download a story as a complete webpage given a url to the story
    or a :class:`BeautifulSoup <BeautifulSoup>` object

    css, js and images are saved locally and its respective
    src or href attributes replaced to match the downloaded resource

    Output is a modified .html file and a folder containing the saved
    resources

    :param source: A string url or :class:`BeautifulSoup` object
    :param out_dir: The directory to save the download page
    :param domain: In case of relative paths, the domain is needed to get absolute
                   url. Make sure it does not have a leading '/' and has the protocol
                   (eg 'http://')
    :return: The path to the downloaded html file
    """

    if isinstance(source, str):
        logger.info('Downloading %s', source)
        if not domain:
            parsed_uri = urlparse(source)
            domain = '{uri.scheme}://{uri.netloc}'.format(uri=parsed_uri)
        try:
            res = requests.get(source)
        except requests.RequestException as e:
            logger.error(e)
            raise e
        source = BeautifulSoup(res.content, settings.bs4_parser)
    elif isinstance(source, BeautifulSoup):
        if not domain:
            logger.error('Domain needed when source is a BeautifulSoup object')
        logger.info('Downloading %s', source.find('title').text)

    title = source.find('title').text

    if domain.endswith(r'/'):
        domain = domain[0:-1]

    # replace invalid characters for new directory names
    resources = os.path.join(out_dir, sanitize_path(title) + '_files')

    if os.path.exists(resources):
        shutil.rmtree(resources)

    os.makedirs(resources)

    html_file_path = os.path.join(out_dir, sanitize_path(title) + '.html')

    # save css
    __save(source, resources, 'link', 'href', __is_css_tag, domain)

    # save js
    __save(source, resources, 'script', 'src', __is_script_tag, domain)

    # save images
    __save(source, resources, 'img', 'src', __is_img_tag, domain)

    # save html file with changes
    with open(html_file_path, "wb") as file:
        file.write(source.prettify('utf-8', formatter='html'))

    return os.path.abspath(html_file_path)


def __save(soup: BeautifulSoup, resources: str, tag: str, attribute: str, validation: Callable[..., bool], domain=''):
    """
    Save a certain type of resource given the tag the contains
    a link to the resource, and the attribute that holds the link

    :param soup: :class:`BeautifulSoup <BeautifulSoup>` object to use
    :param resources: absolute path to the directory to store saved resources
    :param tag:
    :param attribute:
    :param domain: The domain without the leading '/'
    :return:
    """
    for t in soup.find_all(tag):
        if validation(t):
            # download resource
            orig_src = ''
            try:
                orig_src = t.get(attribute)
                if orig_src.startswith(r'//'):
                    orig_src = 'http:' + orig_src
                elif orig_src.startswith(r'/'):
                    orig_src = domain + orig_src

                encoded_src = orig_src.replace(' ', '%20')  # encode space in url
            except TypeError:
                continue

            if not orig_src:
                continue

            src_filename = sanitize_path(os.path.basename(orig_src))
            new_filename = os.path.join(resources, src_filename)

            # check that a file with the same filename exists
            i = 2
            while os.path.exists(new_filename):
                new_filename = os.path.join(resources, src_filename + '_{}'.format(i)).replace(' ', '_')
                i += 1

            try:
                urlretrieve(encoded_src, new_filename)
            except:
                continue

            # replace img src with local image
            # relative_filename used in html uses '/' as opposed to windows '\'
            resources_parent_dir = os.path.dirname(resources)
            relative_filename = '.' + new_filename.replace(resources_parent_dir, '').replace('\\', '/')

            # replace link to saved resource
            t[attribute] = relative_filename


def __is_css_tag(tag: bs4.element.Tag) -> bool:
    try:
        is_link_tag, has_rel, is_css, has_href = False, False, False, False

        try:
            is_link_tag = tag.name == 'link'
        except AttributeError:
            pass

        try:
            has_rel = 'stylesheet' in tag.get('rel')  # get() returns a list
        except TypeError:  # TypeError: argument of type 'NoneType' is not iterable
            pass

        try:
            is_css = 'text/css' in tag.get('type')
        except TypeError:
            pass

        has_href = tag.has_attr('href')

        return is_link_tag and (has_rel or is_css) and has_href
    except:
        return False


def __is_script_tag(tag: bs4.element.Tag) -> bool:
    try:
        is_script_tag = False
        try:
            is_script_tag = tag.name == 'script'
        except AttributeError:
            pass

        has_src = tag.has_attr('src')

        return is_script_tag and has_src
    except:
        return False


def __is_img_tag(tag: bs4.element.Tag) -> bool:
    try:
        is_img_tag, has_src = False, False
        try:
            is_img_tag = 'img' in tag.name
        except AttributeError:
            pass

        has_src = tag.has_attr('src')

        return is_img_tag and has_src
    except:
        return False


def download_wget(url: str, out_dir: str, extra_args=[], supress=True) -> str:
    """
    Download a page using wget

    :param url: The url of the page to download
    :param out_dir: The directory to save
    :param supress: True to not print wget output
    :return: The path to the downloaded html file
    """
    logger.info('WGET: Downloading %s', url)

    # create directories if it does not exists
    if not os.path.isdir(out_dir):
        os.makedirs(out_dir)

    wget = settings.wget_bin(sys.platform)
    config = settings.wget_config(sys.platform)
    args = ['-P', os.path.abspath(out_dir), '--config', config]
    args += extra_args
    args += [url]

    if supress:
        retval = call([wget] + args, stdout=DEVNULL, stderr=DEVNULL)
    else:
        retval = call([wget] + args)

    # wget error codes
    # https://www.gnu.org/software/wget/manual/html_node/Exit-Status.html
    if retval not in [0, 8]:
        raise WgetError("")

    # the downloaded html should match exactly the url scheme
    html_dir = os.path.join(out_dir, os.path.dirname(strip_protocol(url)))

    # get html
    html_files = glob.glob(os.path.join(html_dir, '*.html'))

    if not html_files:
        raise WgetError("Unable to find downloaded html")

    # try finding file using url
    try:
        maybe_filename = url.rsplit('/', 1)[-1]
        html = [x for x in html_files if maybe_filename in x][0]
    except IndexError:
        # return latest html by creation date as last resort
        html = max(html_files, key=os.path.getctime)

    return html


def find_dois(url: str, regex=settings.DOI_REGEX_LENIENT) -> List[str]:
    """
    Find doi in a page by meta tag

    :param url: the url of the page you want to look for doi
    :param regex: custom regex to use to search the doi
    :return: A pair containing the url and a list of dois
    """
    logger = logging.getLogger(__name__ + '.find_dois')

    if not url:
        logger.error('Url is empty.')
        raise ValueError('Url is empty')
    elif INVALID_DOI_URLS.search(url):
        return []
    elif tldextract.extract(url).domain == 'doi':
        # http://dx.doi.org/<doi number>
        return settings.DOI_REGEX_LENIENT.search(url).group()

    html = None
    try:
        html = requests.get(url).content
    except requests.exceptions.RequestException as e:
        logger.error(e)
        return []

    soup = BeautifulSoup(html, settings.bs4_parser)

    # https://help.altmetric.com/support/solutions/articles/6000141419-what-metadata-is-required-to-track-our-content-
    # look inside meta tags
    doi = None
    meta_tags = ['citation_doi',
                 'doi',
                 'DC.Identifier',
                 'DC.DOI',
                 'DC.Identifier.DOI',
                 'DOIs',
                 'bepress_citation_doi',
                 'rft_id']

    for tag in meta_tags:
        meta_doi = soup.find('meta', {'name': tag})
        if meta_doi:
            # remove 'doi:', eg doi:10.1038/sdata.2015.8 -> 10.1038/sdata.2015.8
            match = regex.search(meta_doi.get('content'))
            if match:
                doi = match.group()
                break

    if doi:
        logger.debug('Found DOI from %s: %s', url, doi)

    return doi


def get_scraper(scraper_name: str) -> scrapers.SelectorScraper:
    """
    Get a scraper by name
    
    :return: The scraper class
    """
    for name, obj in inspect.getmembers(scrapers, inspect.isclass):
        if scraper_name.lower() == name.lower().replace('scraper', ''):
            return obj


def get_all_scrapers() -> List[Tuple[str, scrapers.AbstractScraper]]:
    filter = ['AbstractScraper', 'Selectors', 'SelectorScraper', 'TemplateScraper']
    return [(name.lower().replace('scraper', ''), obj) for name, obj in inspect.getmembers(scrapers, inspect.isclass) if name not in filter]


class WgetError(Exception):
    pass
