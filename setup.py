from setuptools import setup

import sys
if sys.version_info < (3,5):
    sys.exit('Minimum Python 3.5 required.')

setup(
    name='prscrape',
    version='1.0.0',
    description='Scrape NZ university news and stories.',
    author='Lindsen Cruz',
    author_email='cheboncruz@gmaill.com',
    url='https://gitlab.com/silentcon/prscrape',
    packages=['prscrape'],
    install_requires=[
        'beautifulsoup4',
        'click',
        'lxml',
	'tldextract',
        'requests'
    ],
    include_package_data=True,
    entry_points={
        'console_scripts': [
            'prscrape = prscrape.__main__:main'
        ]
    }
)
